﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace Dw.Shared.Web.Context
{
    public class AuthorService
    {
        private readonly IHttpContextAccessor _context;
        public AuthorService(IHttpContextAccessor context)
        {
            _context = context;
        }

        public int GetId()
        {
            var userClaim = _context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);
            return userClaim == null ? 0 : int.Parse(userClaim.Value);
        }

        public string GetName()
        {
            return _context.HttpContext.User.Identity.Name;
        }
    }
}
