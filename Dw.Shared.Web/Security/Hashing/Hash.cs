﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Linq;

namespace Dw.Shared.Web.Security.Hashing
{
    public class Hash
    {
        public static byte[] Create(string value, byte[] salt)
        {
            // derive a 256-bit subkey (use HMACSHA512 with 10,000 iterations)
            var hashed = KeyDerivation.Pbkdf2(value, salt, KeyDerivationPrf.HMACSHA512, 10000, 32);
            return hashed;
        }

        public static bool VerifyHashed(string value, byte[] salt, byte[] hash)
            => Create(value, salt).SequenceEqual(hash);
    }
}
