﻿using System.Security.Cryptography;

namespace Dw.Shared.Web.Security.Hashing
{
    public class Salt
    {
        public static byte[] Create()
        {
            var salt = new byte[16];
            
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }
    }
}
