﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Dw.Shared.Core.Collections.Extensions
{
    public static class LinqExtensions
    {
        public static IEnumerable<dynamic> Sort(this IQueryable collection, string sortBy, bool reverse = false)
        {
            if(sortBy != null)
            {
                return collection.OrderBy(sortBy + (reverse ? " descending" : "")).AsEnumerable();
            }
            return collection.AsEnumerable();
        }
    }
}
