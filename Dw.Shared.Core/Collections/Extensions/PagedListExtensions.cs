﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dw.Shared.Core.Collections.Extensions
{
    public interface IPagingInfo
    {
        /// <summary>
        /// Get total number of records
        /// </summary>
        int Total { get; }

        /// <summary>
        /// Offset
        /// </summary>
        int Offset { get; }

        /// <summary>
        /// Limit
        /// </summary>
        int Limit { get; }

        /// <summary>
        /// Get offset and limit of records
        /// </summary>
        int Returned { get; }
    }

    public class PagingInfo : IPagingInfo
    {
        public PagingInfo(
           int total, int offset, int limit, int returned)
        {
            Total = total;
            Offset = offset;
            Limit = limit;
            Returned = returned;
        }

        public int Total { get; }

        public int Offset { get; }

        public int Limit { get; }

        public int Returned { get; }
    }

    public class PagedResult<T>
    {
        /// <summary>
        /// Paging info
        /// </summary>
        public IPagingInfo Paging { get; }

        /// <summary>
        /// Items
        /// </summary>
        public IList<T> Items { get; }

        public PagedResult(IPagingInfo pagingInfo, IList<T> items)
        {
            Paging = pagingInfo;
            Items = items;
        }
    }

    public static class PagedListExtensions
    {
        public static PagedResult<T> GetPaged<T>(
            this IEnumerable<T> query,
            int? offset, 
            int? limit) where T : class
        {
            var skip = offset ?? 0;
            var take = limit ?? int.MaxValue;

            var items = query
                .Skip(skip)
                .Take(take)
                .ToList();

            var pagedResult = new PagedResult<T>(
                new PagingInfo(
                    query.Count(),
                    skip, take,
                    items.Count()),
                items);

            return pagedResult;
        }
    }
}
