﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dw.Shared.Core.Domain.Repositories
{
    public interface IRedisObjectStore<T>
    {
        /// <summary>
        /// Get an object stored in redis by key
        /// </summary>
        /// <param name="key">The key used to stroe object</param>
        T Get(string key);

        /// <summary>
        /// Add an object in redis
        /// Returns true if add object
        /// </summary>
        /// <param name="key">The key to stroe object against</param>
        /// <param name="payload">The object to store</param>
        bool Add(string key, T payload, TimeSpan? expire);

        /// <summary>
        /// Delete an object from redis using a key
        /// Returns true if remove object
        /// </summary>
        /// <param name="key">The key the object is stored using</param>
        bool Remove(string key);

        /// <summary>
        /// Returns true if key exists
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool HasKey(string key);
    }
}
