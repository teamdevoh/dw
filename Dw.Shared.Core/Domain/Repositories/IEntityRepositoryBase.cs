﻿using EFCore.BulkExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dw.Shared.Core.Domain.Repositories
{
    public interface IEntityRepositoryBase<TEntity>
    {
        void Add(TEntity entity);

        void AddRange(IEnumerable<TEntity> entities);

        void AddRange(IEnumerable<TEntity> entities, Expression<Func<TEntity, bool>> filterExpression);

        void Update(TEntity entity);

        void Remove(TEntity entity);

        void RemoveRange(Expression<Func<TEntity, bool>> filterExpression);

        TEntity GetById(params object[] ids);

        ValueTask<TEntity> GetByIdAsync(params object[] ids);

        TEntity GetFirst(params Expression<Func<TEntity, object>>[] navigationProperties);

        TEntity GetFirst(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties);

        Task<TEntity> GetFirstAsync(params Expression<Func<TEntity, object>>[] navigationProperties);

        Task<TEntity> GetFirstAsync(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties);

        TEntity GetLast(params Expression<Func<TEntity, object>>[] navigationProperties);

        TEntity GetLast(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties);

        Task<TEntity> GetLastAsync(params Expression<Func<TEntity, object>>[] navigationProperties);

        Task<TEntity> GetLastAsync(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties);

        IQueryable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] navigationProperties);

        Task<List<TEntity>> GetAllAsync(params Expression<Func<TEntity, object>>[] navigationProperties);

        IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties);

        Task<List<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties);

        TEntity GetSingle(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties);

        Task<TEntity> GetSingleAsync(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties);

        bool IsExists(Expression<Func<TEntity, bool>> filterExpression);

        Task<bool> IsExistsAsync(Expression<Func<TEntity, bool>> filterExpression);
    }
}
