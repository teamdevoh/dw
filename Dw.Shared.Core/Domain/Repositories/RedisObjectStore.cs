﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;

namespace Dw.Shared.Core.Domain.Repositories
{
    public class RedisObjectStore<T> : IRedisObjectStore<T>
    {
        private readonly IDatabase _db;

        public RedisObjectStore(IDatabase db)
        {
            _db = db;
        }

        public T Get(string key)
        {
            VerifyKey(key);
            var payload = _db.StringGet(key);
            if (payload.IsNullOrEmpty)
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(_db.StringGet(key));
        }

        public bool Add(string key, T payload)
        {
            VerifyKey(key);
            return Add(key, payload, null);
        }

        public bool Add(string key, T payload, TimeSpan? expire)
        {
            VerifyKey(key);
            return _db.StringSet(key, JsonConvert.SerializeObject(payload), expire);
        }

        public bool Remove(string key)
        {
            VerifyKey(key);
            return _db.KeyDelete(key);
        }

        public bool HasKey(string key)
        {
            return _db.KeyExists(key);
        }

        private void VerifyKey(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentException("invalid key");
        }

    }
}
