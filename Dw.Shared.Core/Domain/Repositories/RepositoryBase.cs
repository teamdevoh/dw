﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dw.Shared.Core.Domain.Repositories
{
    public abstract class RepositoryBase<TContext> : IRepositoryInjection where TContext : DbContext
    {
        protected ILogger Logger { get; private set; }
        protected TContext Context { get; private set; }

        protected RepositoryBase(TContext context)
        {
            Context = context;
        }

        public IRepositoryInjection SetContext(DbContext context)
        {
            Context = (TContext)context;
            return this;
        }
    }
}
