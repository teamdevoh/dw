﻿using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Dw.Shared.Core.Domain.Repositories
{
    public class EntityRepositoryBase<TContext, TEntity> : RepositoryBase<TContext>, IEntityRepositoryBase<TEntity> where TEntity : class where TContext : DbContext
    {
        public EntityRepositoryBase(TContext context) : base(context)
        {

        }

        private IQueryable<TEntity> Include(params Expression<Func<TEntity, object>>[] navigationProperties)
        {
            try
            {
                IQueryable<TEntity> dbQuery = Context.Set<TEntity>();
                foreach (Expression<Func<TEntity, object>> navigationProperty in navigationProperties)
                    dbQuery = dbQuery.Include(navigationProperty);
                return dbQuery;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void Add(TEntity entity)
        {
            if (entity == null) throw new InvalidOperationException("Unable to add a null entity to the repository.");
            Context.Set<TEntity>().Add(entity);
        }

        public virtual void AddRange(IEnumerable<TEntity> entities)
        {
            if (entities == null) throw new InvalidOperationException("Unable to add a null entity to the repository.");
            Context.Set<TEntity>().AddRange(entities);
        }

        public virtual void AddRange(IEnumerable<TEntity> entities, Expression<Func<TEntity, bool>> filterExpression)
        {
            if (entities == null) throw new InvalidOperationException("Unable to add a null entity to the repository.");
            Context.Set<TEntity>().AddRange(entities);
        }

        public virtual void Update(TEntity entity)
        {
            if (entity == null) throw new InvalidOperationException("Unable to attach a null entity to the repository.");
            Context.Set<TEntity>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Remove(TEntity entity)
        {
            if (entity == null) throw new InvalidOperationException("Unable to remove a null entity to the repository.");
            Context.Set<TEntity>().Remove(entity);
        }

        public virtual void RemoveRange(Expression<Func<TEntity, bool>> filterExpression)
        {
            var entities = Context.Set<TEntity>().Where(filterExpression).ToList();
            if (entities == null) throw new InvalidOperationException("Unable to remove a null entity to the repository.");
            Context.Set<TEntity>().RemoveRange(entities);
        }

        public virtual TEntity GetById(params object[] ids) 
            => Context.Set<TEntity>().Find(ids);

        public virtual ValueTask<TEntity> GetByIdAsync(params object[] ids)
            => Context.Set<TEntity>().FindAsync(ids);

        public virtual TEntity GetFirst(params Expression<Func<TEntity, object>>[] navigationProperties) 
            => Include(navigationProperties).AsNoTracking().FirstOrDefault();

        public virtual TEntity GetFirst(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties) 
            => Include(navigationProperties).AsNoTracking().FirstOrDefault(filterExpression);

        public virtual Task<TEntity> GetFirstAsync(params Expression<Func<TEntity, object>>[] navigationProperties) 
            => Include(navigationProperties).AsNoTracking().FirstOrDefaultAsync();

        public virtual Task<TEntity> GetFirstAsync(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties) 
            => Include(navigationProperties).AsNoTracking().FirstOrDefaultAsync(filterExpression);

        public virtual TEntity GetLast(params Expression<Func<TEntity, object>>[] navigationProperties) 
            => Include(navigationProperties).AsNoTracking().LastOrDefault();

        public virtual TEntity GetLast(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties)
            => Include(navigationProperties).AsNoTracking().LastOrDefault(filterExpression);

        public virtual Task<TEntity> GetLastAsync(params Expression<Func<TEntity, object>>[] navigationProperties)
            => Include(navigationProperties).AsNoTracking().LastOrDefaultAsync();

        public virtual Task<TEntity> GetLastAsync(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties)
            => Include(navigationProperties).AsNoTracking().LastOrDefaultAsync(filterExpression);

        public virtual IQueryable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] navigationProperties)
            => Include(navigationProperties).AsNoTracking();

        public virtual async Task<List<TEntity>> GetAllAsync(params Expression<Func<TEntity, object>>[] navigationProperties) 
            => await Include(navigationProperties).AsNoTracking().ToListAsync();

        public virtual IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties)
            => Include(navigationProperties).AsNoTracking().Where(filterExpression);

        public virtual async Task<List<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties)
            => await Include(navigationProperties).AsNoTracking().Where(filterExpression).ToListAsync();

        public virtual TEntity GetSingle(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties)
            => Include(navigationProperties).AsNoTracking().SingleOrDefault(filterExpression);

        public virtual Task<TEntity> GetSingleAsync(Expression<Func<TEntity, bool>> filterExpression, params Expression<Func<TEntity, object>>[] navigationProperties)
            => Include(navigationProperties).AsNoTracking().SingleOrDefaultAsync(filterExpression);

        public virtual bool IsExists(Expression<Func<TEntity, bool>> filterExpression)
            => Context.Set<TEntity>().AsNoTracking().Any(filterExpression);

        public virtual Task<bool> IsExistsAsync(Expression<Func<TEntity, bool>> filterExpression)
            => Context.Set<TEntity>().AsNoTracking().AnyAsync(filterExpression);
    }
}
