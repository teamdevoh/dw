﻿using Dw.Shared.Core.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dw.Shared.Core.Domain.Uow
{
    public interface IUnitOfWork
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();
        DbContext GetDbContext();
        IEntityRepositoryBase<TEntity> GetRep<TEntity>();
    }
}
