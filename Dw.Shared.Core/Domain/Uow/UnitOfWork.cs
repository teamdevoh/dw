﻿using Dw.Shared.Core.Domain.Repositories;
using Dw.Shared.Core.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Dw.Shared.Core.Domain.Uow
{
    public class UnitOfWork<TContext> : IUnitOfWork, IDisposable 
        where TContext : DbContext
    {
        private readonly IServiceProvider _svcProvider;
        private TContext _ctx;

        public UnitOfWork(TContext ctx, IServiceProvider svcProvider)
        {
            ctx.ChangeTracker.AutoDetectChangesEnabled = false;
            
            _ctx = ctx;
            _svcProvider = svcProvider;

            _ctx.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public DbContext GetDbContext()
        {
            try
            {
                return _ctx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public IEntityRepositoryBase<TEntity> GetRep<TEntity>()
        {
            var repositoryType = typeof(IEntityRepositoryBase<TEntity>);
            var repository = (IEntityRepositoryBase<TEntity>)_svcProvider.GetService(repositoryType);
            if (repository == null)
            {
                throw new RepositoryNotFoundException(repositoryType.Name, String.Format("Repository {0} not found in the IOC container. Check if it is registered during startup.", repositoryType.Name));
            }

            ((IRepositoryInjection)repository).SetContext(_ctx);

            return repository;
        }

        public int SaveChanges()
        {
            using (var dbCtxTxn = _ctx.Database.BeginTransaction())
            {
                try
                {
                    CheckDisposed();
                    var result = _ctx.SaveChanges();
                    dbCtxTxn.Commit();

                    return result;
                }
                catch (DbUpdateException ex)
                {
                    dbCtxTxn.Rollback();
                    throw ex;
                }
                catch (NotSupportedException ex)
                {
                    dbCtxTxn.Rollback();
                    throw ex;
                }
                catch (ObjectDisposedException ex)
                {
                    dbCtxTxn.Rollback();
                    throw ex;
                }
                catch (InvalidOperationException ex)
                {
                    dbCtxTxn.Rollback();
                    throw ex;
                }
            }
        }


        public async Task<int> SaveChangesAsync()
        {
            using (var dbCtxTxn = _ctx.Database.BeginTransaction())
            {
                try
                {
                    CheckDisposed();
                    var result = await _ctx.SaveChangesAsync();
                    dbCtxTxn.Commit();

                    return result;
                }
                catch (DbUpdateException ex)
                {
                    dbCtxTxn.Rollback();
                    throw ex;
                }
                catch (NotSupportedException ex)
                {
                    dbCtxTxn.Rollback();
                    throw ex;
                }
                catch (ObjectDisposedException ex)
                {
                    dbCtxTxn.Rollback();
                    throw ex;
                }
                catch (InvalidOperationException ex)
                {
                    dbCtxTxn.Rollback();
                    throw ex;
                }
            }
        }

        protected bool _isDisposed;

        protected void CheckDisposed()
        {
            if (_isDisposed) throw new ObjectDisposedException("The UnitOfWork is already disposed and cannot be used anymore.");
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (_ctx != null)
                    {
                        _ctx.Dispose();
                        _ctx = null;
                    }
                }
            }
            _isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }

    }
}
