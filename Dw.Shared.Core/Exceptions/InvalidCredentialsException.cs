﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dw.Shared.Core.Exceptions
{
    public class InvalidCredentialsException : BaseException
    {
        public InvalidCredentialsException(string message) : base(message)
        {
        }

        public InvalidCredentialsException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    
}
