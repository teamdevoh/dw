﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dw.Shared.Core.Exceptions
{
    public class UserNotFoundException : BaseException
    {
        public UserNotFoundException(string message) : base(message)
        {
        }

        public UserNotFoundException(string message, Exception innerException) : base(message, innerException)
		{
        }
    }
}
