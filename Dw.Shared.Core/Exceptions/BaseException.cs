﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dw.Shared.Core.Exceptions
{
    public class BaseException : Exception
    {
        public BaseException()
        { }

        public BaseException(string message)
            : base(message)
        { }

        public BaseException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
