﻿using Dw.Shared.Core.Domain.Context;
using Dw.Shared.Core.Domain.Repositories;
using Dw.Shared.Core.Domain.Uow;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;

namespace Dw.Shared.Core.StartUp
{
    public static class ServiceCollectionExtentions
    {
        public static IServiceCollection AddDataAccess<TContext>(this IServiceCollection services) where TContext : EntityContextBase<TContext>
        {
            RegisterDataAccess<TContext>(services);
            return services;
        }

        private static void RegisterDataAccess<TContext>(IServiceCollection services) where TContext : EntityContextBase<TContext>
        {
            services.AddTransient<IUnitOfWork, UnitOfWork<TContext>>();
            services.AddTransient(typeof(IEntityRepositoryBase<>), typeof(GenericEntityRepository<>));
            services.AddTransient<IEntityContext, TContext>();
        }

        private static void ValidateMandatoryField(string field, string fieldName)
        {
            if (field == null) throw new ArgumentNullException(fieldName, $"{fieldName} cannot be null.");
            if (field.Trim() == String.Empty) throw new ArgumentException($"{fieldName} cannot be empty.", fieldName);
        }

        private static void ValidateMandatoryField(object field, string fieldName)
        {
            if (field == null) throw new ArgumentNullException(fieldName, $"{fieldName} cannot be null.");
        }
    }
}
